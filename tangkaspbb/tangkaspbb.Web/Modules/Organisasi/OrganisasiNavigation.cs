﻿using Serenity.Navigation;
using MyPages = tangkaspbb.Organisasi.Pages;

[assembly: NavigationLink(int.MaxValue, "Organisasi/Institutions", typeof(MyPages.InstitutionsController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Organisasi/Officials", typeof(MyPages.OfficialsController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Organisasi/Positions", typeof(MyPages.PositionsController), icon: null)]