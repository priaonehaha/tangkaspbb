﻿
namespace tangkaspbb.Organisasi.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.PositionsRow))]
    public class PositionsController : Controller
    {
        [Route("Organisasi/Positions")]
        public ActionResult Index()
        {
            return View("~/Modules/Organisasi/Positions/PositionsIndex.cshtml");
        }
    }
}