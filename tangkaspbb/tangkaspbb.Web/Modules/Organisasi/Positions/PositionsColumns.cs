﻿
namespace tangkaspbb.Organisasi.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Organisasi.Positions")]
    [BasedOnRow(typeof(Entities.PositionsRow), CheckNames = true)]
    public class PositionsColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Positionid { get; set; }
        [EditLink]
        public String Positionname { get; set; }
        public String InstitutionidInstitutionname { get; set; }
        public Int32 Level { get; set; }
        public String SuperPositionname { get; set; }
    }
}