﻿
namespace tangkaspbb.Organisasi {

    @Serenity.Decorators.registerClass()
    export class PositionsDialog extends Serenity.EntityDialog<PositionsRow, any> {
        protected getFormKey() { return PositionsForm.formKey; }
        protected getIdProperty() { return PositionsRow.idProperty; }
        protected getLocalTextPrefix() { return PositionsRow.localTextPrefix; }
        protected getNameProperty() { return PositionsRow.nameProperty; }
        protected getService() { return PositionsService.baseUrl; }

        protected form = new PositionsForm(this.idPrefix);

    }
}