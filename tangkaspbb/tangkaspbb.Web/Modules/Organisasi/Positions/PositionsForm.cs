﻿
namespace tangkaspbb.Organisasi.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Organisasi.Positions")]
    [BasedOnRow(typeof(Entities.PositionsRow), CheckNames = true)]
    public class PositionsForm
    {
        public String Positionname { get; set; }
        public Int32 Institutionid { get; set; }
        public Int32 Level { get; set; }
        public Int32 Super { get; set; }
    }
}