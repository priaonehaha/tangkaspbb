﻿
namespace tangkaspbb.Organisasi.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Organisasi"), TableName("[public].[positions]")]
    [DisplayName("Positions"), InstanceName("Positions")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class PositionsRow : Row, IIdRow, INameRow
    {
        [DisplayName("Positionid"), Column("positionid"), PrimaryKey]
        public Int32? Positionid
        {
            get { return Fields.Positionid[this]; }
            set { Fields.Positionid[this] = value; }
        }

        [DisplayName("Positionname"), Column("positionname"), QuickSearch]
        public String Positionname
        {
            get { return Fields.Positionname[this]; }
            set { Fields.Positionname[this] = value; }
        }

        [DisplayName("Institutionid"), Column("institutionid"), ForeignKey("[public].[institutions]", "institutionid"), LeftJoin("jInstitutionid"), TextualField("InstitutionidInstitutionname")]
        public Int32? Institutionid
        {
            get { return Fields.Institutionid[this]; }
            set { Fields.Institutionid[this] = value; }
        }

        [DisplayName("Level"), Column("level")]
        public Int32? Level
        {
            get { return Fields.Level[this]; }
            set { Fields.Level[this] = value; }
        }

        [DisplayName("Super"), Column("super"), ForeignKey("[public].[positions]", "positionid"), LeftJoin("jSuper"), TextualField("SuperPositionname")]
        public Int32? Super
        {
            get { return Fields.Super[this]; }
            set { Fields.Super[this] = value; }
        }

        [DisplayName("Institutionid Institutionname"), Expression("jInstitutionid.[institutionname]")]
        public String InstitutionidInstitutionname
        {
            get { return Fields.InstitutionidInstitutionname[this]; }
            set { Fields.InstitutionidInstitutionname[this] = value; }
        }

        [DisplayName("Institutionid Level"), Expression("jInstitutionid.[level]")]
        public Int32? InstitutionidLevel
        {
            get { return Fields.InstitutionidLevel[this]; }
            set { Fields.InstitutionidLevel[this] = value; }
        }

        [DisplayName("Institutionid Super"), Expression("jInstitutionid.[super]")]
        public Int32? InstitutionidSuper
        {
            get { return Fields.InstitutionidSuper[this]; }
            set { Fields.InstitutionidSuper[this] = value; }
        }

        [DisplayName("Super Positionname"), Expression("jSuper.[positionname]")]
        public String SuperPositionname
        {
            get { return Fields.SuperPositionname[this]; }
            set { Fields.SuperPositionname[this] = value; }
        }

        [DisplayName("Super Institutionid"), Expression("jSuper.[institutionid]")]
        public Int32? SuperInstitutionid
        {
            get { return Fields.SuperInstitutionid[this]; }
            set { Fields.SuperInstitutionid[this] = value; }
        }

        [DisplayName("Super Level"), Expression("jSuper.[level]")]
        public Int32? SuperLevel
        {
            get { return Fields.SuperLevel[this]; }
            set { Fields.SuperLevel[this] = value; }
        }

        [DisplayName("Super"), Expression("jSuper.[super]")]
        public Int32? Super1
        {
            get { return Fields.Super1[this]; }
            set { Fields.Super1[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Positionid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Positionname; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public PositionsRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Positionid;
            public StringField Positionname;
            public Int32Field Institutionid;
            public Int32Field Level;
            public Int32Field Super;

            public StringField InstitutionidInstitutionname;
            public Int32Field InstitutionidLevel;
            public Int32Field InstitutionidSuper;

            public StringField SuperPositionname;
            public Int32Field SuperInstitutionid;
            public Int32Field SuperLevel;
            public Int32Field Super1;
        }
    }
}
