﻿
namespace tangkaspbb.Organisasi {

    @Serenity.Decorators.registerClass()
    export class PositionsGrid extends Serenity.EntityGrid<PositionsRow, any> {
        protected getColumnsKey() { return 'Organisasi.Positions'; }
        protected getDialogType() { return PositionsDialog; }
        protected getIdProperty() { return PositionsRow.idProperty; }
        protected getLocalTextPrefix() { return PositionsRow.localTextPrefix; }
        protected getService() { return PositionsService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}