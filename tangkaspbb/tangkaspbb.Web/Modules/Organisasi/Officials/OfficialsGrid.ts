﻿
namespace tangkaspbb.Organisasi {

    @Serenity.Decorators.registerClass()
    export class OfficialsGrid extends Serenity.EntityGrid<OfficialsRow, any> {
        protected getColumnsKey() { return 'Organisasi.Officials'; }
        protected getDialogType() { return OfficialsDialog; }
        protected getIdProperty() { return OfficialsRow.idProperty; }
        protected getLocalTextPrefix() { return OfficialsRow.localTextPrefix; }
        protected getService() { return OfficialsService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}