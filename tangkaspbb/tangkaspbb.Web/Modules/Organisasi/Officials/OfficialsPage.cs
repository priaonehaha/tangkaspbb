﻿
namespace tangkaspbb.Organisasi.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.OfficialsRow))]
    public class OfficialsController : Controller
    {
        [Route("Organisasi/Officials")]
        public ActionResult Index()
        {
            return View("~/Modules/Organisasi/Officials/OfficialsIndex.cshtml");
        }
    }
}