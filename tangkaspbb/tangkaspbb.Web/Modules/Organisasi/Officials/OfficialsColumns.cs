﻿
namespace tangkaspbb.Organisasi.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Organisasi.Officials")]
    [BasedOnRow(typeof(Entities.OfficialsRow), CheckNames = true)]
    public class OfficialsColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Officialid { get; set; }
        [EditLink]
        public String Officialname { get; set; }
        public String InstitutionidInstitutionname { get; set; }
        public DateTime Servedfrom { get; set; }
        public DateTime Servedto { get; set; }
        public Int32 Certificateid { get; set; }
        public String PositionidPositionname { get; set; }
        public Int32 Signatureid { get; set; }
    }
}