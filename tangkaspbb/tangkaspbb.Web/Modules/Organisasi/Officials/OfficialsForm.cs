﻿
namespace tangkaspbb.Organisasi.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Organisasi.Officials")]
    [BasedOnRow(typeof(Entities.OfficialsRow), CheckNames = true)]
    public class OfficialsForm
    {
        public String Officialname { get; set; }
        public Int32 Institutionid { get; set; }
        public DateTime Servedfrom { get; set; }
        public DateTime Servedto { get; set; }
        public Int32 Certificateid { get; set; }
        public Int32 Positionid { get; set; }
        public Int32 Signatureid { get; set; }
    }
}