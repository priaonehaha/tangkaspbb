﻿
namespace tangkaspbb.Organisasi {

    @Serenity.Decorators.registerClass()
    export class OfficialsDialog extends Serenity.EntityDialog<OfficialsRow, any> {
        protected getFormKey() { return OfficialsForm.formKey; }
        protected getIdProperty() { return OfficialsRow.idProperty; }
        protected getLocalTextPrefix() { return OfficialsRow.localTextPrefix; }
        protected getNameProperty() { return OfficialsRow.nameProperty; }
        protected getService() { return OfficialsService.baseUrl; }

        protected form = new OfficialsForm(this.idPrefix);

    }
}