﻿
namespace tangkaspbb.Organisasi.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Organisasi"), TableName("[public].[officials]")]
    [DisplayName("Officials"), InstanceName("Officials")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class OfficialsRow : Row, IIdRow, INameRow
    {
        [DisplayName("Officialid"), Column("officialid"), PrimaryKey]
        public Int32? Officialid
        {
            get { return Fields.Officialid[this]; }
            set { Fields.Officialid[this] = value; }
        }

        [DisplayName("Officialname"), Column("officialname"), QuickSearch]
        public String Officialname
        {
            get { return Fields.Officialname[this]; }
            set { Fields.Officialname[this] = value; }
        }

        [DisplayName("Institutionid"), Column("institutionid"), ForeignKey("[public].[institutions]", "institutionid"), LeftJoin("jInstitutionid"), TextualField("InstitutionidInstitutionname")]
        public Int32? Institutionid
        {
            get { return Fields.Institutionid[this]; }
            set { Fields.Institutionid[this] = value; }
        }

        [DisplayName("Servedfrom"), Column("servedfrom")]
        public DateTime? Servedfrom
        {
            get { return Fields.Servedfrom[this]; }
            set { Fields.Servedfrom[this] = value; }
        }

        [DisplayName("Servedto"), Column("servedto")]
        public DateTime? Servedto
        {
            get { return Fields.Servedto[this]; }
            set { Fields.Servedto[this] = value; }
        }

        [DisplayName("Certificateid"), Column("certificateid")]
        public Int32? Certificateid
        {
            get { return Fields.Certificateid[this]; }
            set { Fields.Certificateid[this] = value; }
        }

        [DisplayName("Positionid"), Column("positionid"), ForeignKey("[public].[positions]", "positionid"), LeftJoin("jPositionid"), TextualField("PositionidPositionname")]
        public Int32? Positionid
        {
            get { return Fields.Positionid[this]; }
            set { Fields.Positionid[this] = value; }
        }

        [DisplayName("Signatureid"), Column("signatureid")]
        public Int32? Signatureid
        {
            get { return Fields.Signatureid[this]; }
            set { Fields.Signatureid[this] = value; }
        }

        [DisplayName("Institutionid Institutionname"), Expression("jInstitutionid.[institutionname]")]
        public String InstitutionidInstitutionname
        {
            get { return Fields.InstitutionidInstitutionname[this]; }
            set { Fields.InstitutionidInstitutionname[this] = value; }
        }

        [DisplayName("Institutionid Level"), Expression("jInstitutionid.[level]")]
        public Int32? InstitutionidLevel
        {
            get { return Fields.InstitutionidLevel[this]; }
            set { Fields.InstitutionidLevel[this] = value; }
        }

        [DisplayName("Institutionid Super"), Expression("jInstitutionid.[super]")]
        public Int32? InstitutionidSuper
        {
            get { return Fields.InstitutionidSuper[this]; }
            set { Fields.InstitutionidSuper[this] = value; }
        }

        [DisplayName("Positionid Positionname"), Expression("jPositionid.[positionname]")]
        public String PositionidPositionname
        {
            get { return Fields.PositionidPositionname[this]; }
            set { Fields.PositionidPositionname[this] = value; }
        }

        [DisplayName("Positionid Institutionid"), Expression("jPositionid.[institutionid]")]
        public Int32? PositionidInstitutionid
        {
            get { return Fields.PositionidInstitutionid[this]; }
            set { Fields.PositionidInstitutionid[this] = value; }
        }

        [DisplayName("Positionid Level"), Expression("jPositionid.[level]")]
        public Int32? PositionidLevel
        {
            get { return Fields.PositionidLevel[this]; }
            set { Fields.PositionidLevel[this] = value; }
        }

        [DisplayName("Positionid Super"), Expression("jPositionid.[super]")]
        public Int32? PositionidSuper
        {
            get { return Fields.PositionidSuper[this]; }
            set { Fields.PositionidSuper[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Officialid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Officialname; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public OfficialsRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Officialid;
            public StringField Officialname;
            public Int32Field Institutionid;
            public DateTimeField Servedfrom;
            public DateTimeField Servedto;
            public Int32Field Certificateid;
            public Int32Field Positionid;
            public Int32Field Signatureid;

            public StringField InstitutionidInstitutionname;
            public Int32Field InstitutionidLevel;
            public Int32Field InstitutionidSuper;

            public StringField PositionidPositionname;
            public Int32Field PositionidInstitutionid;
            public Int32Field PositionidLevel;
            public Int32Field PositionidSuper;
        }
    }
}
