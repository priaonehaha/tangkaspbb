﻿
namespace tangkaspbb.Organisasi.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.InstitutionsRow))]
    public class InstitutionsController : Controller
    {
        [Route("Organisasi/Institutions")]
        public ActionResult Index()
        {
            return View("~/Modules/Organisasi/Institutions/InstitutionsIndex.cshtml");
        }
    }
}