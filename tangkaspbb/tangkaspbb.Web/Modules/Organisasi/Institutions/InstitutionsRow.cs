﻿
namespace tangkaspbb.Organisasi.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Organisasi"), TableName("[public].[institutions]")]
    [DisplayName("Institutions"), InstanceName("Institutions")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class InstitutionsRow : Row, IIdRow, INameRow
    {
        [DisplayName("Institutionid"), Column("institutionid"), PrimaryKey]
        public Int32? Institutionid
        {
            get { return Fields.Institutionid[this]; }
            set { Fields.Institutionid[this] = value; }
        }

        [DisplayName("Institutionname"), Column("institutionname"), QuickSearch]
        public String Institutionname
        {
            get { return Fields.Institutionname[this]; }
            set { Fields.Institutionname[this] = value; }
        }

        [DisplayName("Level"), Column("level")]
        public Int32? Level
        {
            get { return Fields.Level[this]; }
            set { Fields.Level[this] = value; }
        }

        [DisplayName("Super"), Column("super"), ForeignKey("[public].[institutions]", "institutionid"), LeftJoin("jSuper"), TextualField("SuperInstitutionname")]
        public Int32? Super
        {
            get { return Fields.Super[this]; }
            set { Fields.Super[this] = value; }
        }

        [DisplayName("Super Institutionname"), Expression("jSuper.[institutionname]")]
        public String SuperInstitutionname
        {
            get { return Fields.SuperInstitutionname[this]; }
            set { Fields.SuperInstitutionname[this] = value; }
        }

        [DisplayName("Super Level"), Expression("jSuper.[level]")]
        public Int32? SuperLevel
        {
            get { return Fields.SuperLevel[this]; }
            set { Fields.SuperLevel[this] = value; }
        }

        [DisplayName("Super"), Expression("jSuper.[super]")]
        public Int32? Super1
        {
            get { return Fields.Super1[this]; }
            set { Fields.Super1[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Institutionid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Institutionname; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public InstitutionsRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Institutionid;
            public StringField Institutionname;
            public Int32Field Level;
            public Int32Field Super;

            public StringField SuperInstitutionname;
            public Int32Field SuperLevel;
            public Int32Field Super1;
        }
    }
}
