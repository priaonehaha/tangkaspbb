﻿
namespace tangkaspbb.Organisasi {

    @Serenity.Decorators.registerClass()
    export class InstitutionsGrid extends Serenity.EntityGrid<InstitutionsRow, any> {
        protected getColumnsKey() { return 'Organisasi.Institutions'; }
        protected getDialogType() { return InstitutionsDialog; }
        protected getIdProperty() { return InstitutionsRow.idProperty; }
        protected getLocalTextPrefix() { return InstitutionsRow.localTextPrefix; }
        protected getService() { return InstitutionsService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}