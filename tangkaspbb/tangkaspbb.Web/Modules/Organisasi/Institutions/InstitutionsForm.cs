﻿
namespace tangkaspbb.Organisasi.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Organisasi.Institutions")]
    [BasedOnRow(typeof(Entities.InstitutionsRow), CheckNames = true)]
    public class InstitutionsForm
    {
        public String Institutionname { get; set; }
        public Int32 Level { get; set; }
        public Int32 Super { get; set; }
    }
}