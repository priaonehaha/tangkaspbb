﻿
namespace tangkaspbb.Organisasi.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Organisasi.Institutions")]
    [BasedOnRow(typeof(Entities.InstitutionsRow), CheckNames = true)]
    public class InstitutionsColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Institutionid { get; set; }
        [EditLink]
        public String Institutionname { get; set; }
        public Int32 Level { get; set; }
        public String SuperInstitutionname { get; set; }
    }
}