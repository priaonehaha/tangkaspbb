﻿
namespace tangkaspbb.Organisasi {

    @Serenity.Decorators.registerClass()
    export class InstitutionsDialog extends Serenity.EntityDialog<InstitutionsRow, any> {
        protected getFormKey() { return InstitutionsForm.formKey; }
        protected getIdProperty() { return InstitutionsRow.idProperty; }
        protected getLocalTextPrefix() { return InstitutionsRow.localTextPrefix; }
        protected getNameProperty() { return InstitutionsRow.nameProperty; }
        protected getService() { return InstitutionsService.baseUrl; }

        protected form = new InstitutionsForm(this.idPrefix);

    }
}