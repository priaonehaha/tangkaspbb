﻿using Serenity.Navigation;
using MyPages = tangkaspbb.Dokumen.Pages;

[assembly: NavigationLink(int.MaxValue, "Dokumen/Certificates", typeof(MyPages.CertificatesController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Dokumen/Documents", typeof(MyPages.DocumentsController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Dokumen/Documenttypes", typeof(MyPages.DocumenttypesController), icon: null)]
[assembly: NavigationLink(int.MaxValue, "Dokumen/Signatures", typeof(MyPages.SignaturesController), icon: null)]