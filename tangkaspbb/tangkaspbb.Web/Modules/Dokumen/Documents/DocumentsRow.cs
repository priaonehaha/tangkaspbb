﻿
namespace tangkaspbb.Dokumen.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Dokumen"), TableName("[public].[documents]")]
    [DisplayName("Documents"), InstanceName("Documents")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class DocumentsRow : Row, IIdRow, INameRow
    {
        [DisplayName("Documentsid"), Column("documentsid"), PrimaryKey]
        public Int32? Documentsid
        {
            get { return Fields.Documentsid[this]; }
            set { Fields.Documentsid[this] = value; }
        }

        [DisplayName("Documenttypeid"), Column("documenttypeid"), ForeignKey("[public].[documenttypes]", "documenttypeid"), LeftJoin("jDocumenttypeid"), TextualField("DocumenttypeidDocumenttypename")]
        public Int32? Documenttypeid
        {
            get { return Fields.Documenttypeid[this]; }
            set { Fields.Documenttypeid[this] = value; }
        }

        [DisplayName("Documentname"), Column("documentname"), QuickSearch]
        public String Documentname
        {
            get { return Fields.Documentname[this]; }
            set { Fields.Documentname[this] = value; }
        }

        [DisplayName("Documentdescription"), Column("documentdescription")]
        public String Documentdescription
        {
            get { return Fields.Documentdescription[this]; }
            set { Fields.Documentdescription[this] = value; }
        }

        [DisplayName("Originalfilename"), Column("originalfilename")]
        public String Originalfilename
        {
            get { return Fields.Originalfilename[this]; }
            set { Fields.Originalfilename[this] = value; }
        }

        [DisplayName("Originalfilepath"), Column("originalfilepath")]
        public String Originalfilepath
        {
            get { return Fields.Originalfilepath[this]; }
            set { Fields.Originalfilepath[this] = value; }
        }

        [DisplayName("Uploadby"), Column("uploadby")]
        public Int32? Uploadby
        {
            get { return Fields.Uploadby[this]; }
            set { Fields.Uploadby[this] = value; }
        }

        [DisplayName("Uploadedtimestamp"), Column("uploadedtimestamp")]
        public DateTime? Uploadedtimestamp
        {
            get { return Fields.Uploadedtimestamp[this]; }
            set { Fields.Uploadedtimestamp[this] = value; }
        }

        [DisplayName("Savedpath"), Column("savedpath")]
        public String Savedpath
        {
            get { return Fields.Savedpath[this]; }
            set { Fields.Savedpath[this] = value; }
        }

        [DisplayName("Savedfilename"), Column("savedfilename")]
        public String Savedfilename
        {
            get { return Fields.Savedfilename[this]; }
            set { Fields.Savedfilename[this] = value; }
        }

        [DisplayName("Documentstatus"), Column("documentstatus"), Size(1)]
        public String Documentstatus
        {
            get { return Fields.Documentstatus[this]; }
            set { Fields.Documentstatus[this] = value; }
        }

        [DisplayName("Processstarttimestamp"), Column("processstarttimestamp")]
        public DateTime? Processstarttimestamp
        {
            get { return Fields.Processstarttimestamp[this]; }
            set { Fields.Processstarttimestamp[this] = value; }
        }

        [DisplayName("Processendtimestamp"), Column("processendtimestamp")]
        public DateTime? Processendtimestamp
        {
            get { return Fields.Processendtimestamp[this]; }
            set { Fields.Processendtimestamp[this] = value; }
        }

        [DisplayName("Processedby"), Column("processedby")]
        public Int32? Processedby
        {
            get { return Fields.Processedby[this]; }
            set { Fields.Processedby[this] = value; }
        }

        [DisplayName("Processstatus"), Column("processstatus"), Size(1)]
        public String Processstatus
        {
            get { return Fields.Processstatus[this]; }
            set { Fields.Processstatus[this] = value; }
        }

        [DisplayName("Documenttypeid Documenttypename"), Expression("jDocumenttypeid.[documenttypename]")]
        public String DocumenttypeidDocumenttypename
        {
            get { return Fields.DocumenttypeidDocumenttypename[this]; }
            set { Fields.DocumenttypeidDocumenttypename[this] = value; }
        }

        [DisplayName("Documenttypeid Outputsuffix"), Expression("jDocumenttypeid.[outputsuffix]")]
        public String DocumenttypeidOutputsuffix
        {
            get { return Fields.DocumenttypeidOutputsuffix[this]; }
            set { Fields.DocumenttypeidOutputsuffix[this] = value; }
        }

        [DisplayName("Documenttypeid Outputprefix"), Expression("jDocumenttypeid.[outputprefix]")]
        public String DocumenttypeidOutputprefix
        {
            get { return Fields.DocumenttypeidOutputprefix[this]; }
            set { Fields.DocumenttypeidOutputprefix[this] = value; }
        }

        [DisplayName("Documenttypeid Isdisableassembly"), Expression("jDocumenttypeid.[isdisableassembly]")]
        public Stream DocumenttypeidIsdisableassembly
        {
            get { return Fields.DocumenttypeidIsdisableassembly[this]; }
            set { Fields.DocumenttypeidIsdisableassembly[this] = value; }
        }

        [DisplayName("Documenttypeid Isdisablecopy"), Expression("jDocumenttypeid.[isdisablecopy]")]
        public Stream DocumenttypeidIsdisablecopy
        {
            get { return Fields.DocumenttypeidIsdisablecopy[this]; }
            set { Fields.DocumenttypeidIsdisablecopy[this] = value; }
        }

        [DisplayName("Documenttypeid Isdisablefill"), Expression("jDocumenttypeid.[isdisablefill]")]
        public Stream DocumenttypeidIsdisablefill
        {
            get { return Fields.DocumenttypeidIsdisablefill[this]; }
            set { Fields.DocumenttypeidIsdisablefill[this] = value; }
        }

        [DisplayName("Documenttypeid Isdisablemodifyannotations"), Expression("jDocumenttypeid.[isdisablemodifyannotations]")]
        public Stream DocumenttypeidIsdisablemodifyannotations
        {
            get { return Fields.DocumenttypeidIsdisablemodifyannotations[this]; }
            set { Fields.DocumenttypeidIsdisablemodifyannotations[this] = value; }
        }

        [DisplayName("Documenttypeid Isdisablemodifycontent"), Expression("jDocumenttypeid.[isdisablemodifycontent]")]
        public Stream DocumenttypeidIsdisablemodifycontent
        {
            get { return Fields.DocumenttypeidIsdisablemodifycontent[this]; }
            set { Fields.DocumenttypeidIsdisablemodifycontent[this] = value; }
        }

        [DisplayName("Documenttypeid Isdisablescreenreaders"), Expression("jDocumenttypeid.[isdisablescreenreaders]")]
        public Stream DocumenttypeidIsdisablescreenreaders
        {
            get { return Fields.DocumenttypeidIsdisablescreenreaders[this]; }
            set { Fields.DocumenttypeidIsdisablescreenreaders[this] = value; }
        }

        [DisplayName("Documenttypeid Certificationlevel"), Expression("jDocumenttypeid.[certificationlevel]")]
        public Int32? DocumenttypeidCertificationlevel
        {
            get { return Fields.DocumenttypeidCertificationlevel[this]; }
            set { Fields.DocumenttypeidCertificationlevel[this] = value; }
        }

        [DisplayName("Documenttypeid Officialid"), Expression("jDocumenttypeid.[officialid]")]
        public Int32? DocumenttypeidOfficialid
        {
            get { return Fields.DocumenttypeidOfficialid[this]; }
            set { Fields.DocumenttypeidOfficialid[this] = value; }
        }

        [DisplayName("Documenttypeid Printright"), Expression("jDocumenttypeid.[printright]")]
        public Int32? DocumenttypeidPrintright
        {
            get { return Fields.DocumenttypeidPrintright[this]; }
            set { Fields.DocumenttypeidPrintright[this] = value; }
        }

        [DisplayName("Documenttypeid Certificateid"), Expression("jDocumenttypeid.[certificateid]")]
        public Int32? DocumenttypeidCertificateid
        {
            get { return Fields.DocumenttypeidCertificateid[this]; }
            set { Fields.DocumenttypeidCertificateid[this] = value; }
        }

        [DisplayName("Documenttypeid Signatureid"), Expression("jDocumenttypeid.[signatureid]")]
        public Int32? DocumenttypeidSignatureid
        {
            get { return Fields.DocumenttypeidSignatureid[this]; }
            set { Fields.DocumenttypeidSignatureid[this] = value; }
        }

        [DisplayName("Documenttypeid Isencrypted"), Expression("jDocumenttypeid.[isencrypted]")]
        public Stream DocumenttypeidIsencrypted
        {
            get { return Fields.DocumenttypeidIsencrypted[this]; }
            set { Fields.DocumenttypeidIsencrypted[this] = value; }
        }

        [DisplayName("Documenttypeid Userpassword"), Expression("jDocumenttypeid.[userpassword]")]
        public String DocumenttypeidUserpassword
        {
            get { return Fields.DocumenttypeidUserpassword[this]; }
            set { Fields.DocumenttypeidUserpassword[this] = value; }
        }

        [DisplayName("Documenttypeid Ownerpassword"), Expression("jDocumenttypeid.[ownerpassword]")]
        public String DocumenttypeidOwnerpassword
        {
            get { return Fields.DocumenttypeidOwnerpassword[this]; }
            set { Fields.DocumenttypeidOwnerpassword[this] = value; }
        }

        [DisplayName("Documenttypeid Institutionid"), Expression("jDocumenttypeid.[institutionid]")]
        public Int32? DocumenttypeidInstitutionid
        {
            get { return Fields.DocumenttypeidInstitutionid[this]; }
            set { Fields.DocumenttypeidInstitutionid[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Documentsid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Documentname; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DocumentsRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Documentsid;
            public Int32Field Documenttypeid;
            public StringField Documentname;
            public StringField Documentdescription;
            public StringField Originalfilename;
            public StringField Originalfilepath;
            public Int32Field Uploadby;
            public DateTimeField Uploadedtimestamp;
            public StringField Savedpath;
            public StringField Savedfilename;
            public StringField Documentstatus;
            public DateTimeField Processstarttimestamp;
            public DateTimeField Processendtimestamp;
            public Int32Field Processedby;
            public StringField Processstatus;

            public StringField DocumenttypeidDocumenttypename;
            public StringField DocumenttypeidOutputsuffix;
            public StringField DocumenttypeidOutputprefix;
            public StreamField DocumenttypeidIsdisableassembly;
            public StreamField DocumenttypeidIsdisablecopy;
            public StreamField DocumenttypeidIsdisablefill;
            public StreamField DocumenttypeidIsdisablemodifyannotations;
            public StreamField DocumenttypeidIsdisablemodifycontent;
            public StreamField DocumenttypeidIsdisablescreenreaders;
            public Int32Field DocumenttypeidCertificationlevel;
            public Int32Field DocumenttypeidOfficialid;
            public Int32Field DocumenttypeidPrintright;
            public Int32Field DocumenttypeidCertificateid;
            public Int32Field DocumenttypeidSignatureid;
            public StreamField DocumenttypeidIsencrypted;
            public StringField DocumenttypeidUserpassword;
            public StringField DocumenttypeidOwnerpassword;
            public Int32Field DocumenttypeidInstitutionid;
        }
    }
}
