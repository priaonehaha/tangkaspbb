﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class DocumentsDialog extends Serenity.EntityDialog<DocumentsRow, any> {
        protected getFormKey() { return DocumentsForm.formKey; }
        protected getIdProperty() { return DocumentsRow.idProperty; }
        protected getLocalTextPrefix() { return DocumentsRow.localTextPrefix; }
        protected getNameProperty() { return DocumentsRow.nameProperty; }
        protected getService() { return DocumentsService.baseUrl; }

        protected form = new DocumentsForm(this.idPrefix);

    }
}