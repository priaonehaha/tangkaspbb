﻿
namespace tangkaspbb.Dokumen.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.DocumentsRow))]
    public class DocumentsController : Controller
    {
        [Route("Dokumen/Documents")]
        public ActionResult Index()
        {
            return View("~/Modules/Dokumen/Documents/DocumentsIndex.cshtml");
        }
    }
}