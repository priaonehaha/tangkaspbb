﻿
namespace tangkaspbb.Dokumen.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Dokumen.Documents")]
    [BasedOnRow(typeof(Entities.DocumentsRow), CheckNames = true)]
    public class DocumentsColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Documentsid { get; set; }
        public String DocumenttypeidDocumenttypename { get; set; }
        [EditLink]
        public String Documentname { get; set; }
        public String Documentdescription { get; set; }
        public String Originalfilename { get; set; }
        public String Originalfilepath { get; set; }
        public Int32 Uploadby { get; set; }
        public DateTime Uploadedtimestamp { get; set; }
        public String Savedpath { get; set; }
        public String Savedfilename { get; set; }
        public String Documentstatus { get; set; }
        public DateTime Processstarttimestamp { get; set; }
        public DateTime Processendtimestamp { get; set; }
        public Int32 Processedby { get; set; }
        public String Processstatus { get; set; }
    }
}