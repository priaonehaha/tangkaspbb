﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class DocumentsGrid extends Serenity.EntityGrid<DocumentsRow, any> {
        protected getColumnsKey() { return 'Dokumen.Documents'; }
        protected getDialogType() { return DocumentsDialog; }
        protected getIdProperty() { return DocumentsRow.idProperty; }
        protected getLocalTextPrefix() { return DocumentsRow.localTextPrefix; }
        protected getService() { return DocumentsService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}