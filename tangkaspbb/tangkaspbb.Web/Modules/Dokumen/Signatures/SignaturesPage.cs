﻿
namespace tangkaspbb.Dokumen.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.SignaturesRow))]
    public class SignaturesController : Controller
    {
        [Route("Dokumen/Signatures")]
        public ActionResult Index()
        {
            return View("~/Modules/Dokumen/Signatures/SignaturesIndex.cshtml");
        }
    }
}