﻿
namespace tangkaspbb.Dokumen.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Dokumen"), TableName("[public].[signatures]")]
    [DisplayName("Signatures"), InstanceName("Signatures")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class SignaturesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Signatureid"), Column("signatureid"), PrimaryKey]
        public Int32? Signatureid
        {
            get { return Fields.Signatureid[this]; }
            set { Fields.Signatureid[this] = value; }
        }

        [DisplayName("Signaturename"), Column("signaturename"), QuickSearch]
        public String Signaturename
        {
            get { return Fields.Signaturename[this]; }
            set { Fields.Signaturename[this] = value; }
        }

        [DisplayName("Isvisiblesignature"), Column("isvisiblesignature")]
        public Stream Isvisiblesignature
        {
            get { return Fields.Isvisiblesignature[this]; }
            set { Fields.Isvisiblesignature[this] = value; }
        }

        [DisplayName("Imagepath"), Column("imagepath")]
        public String Imagepath
        {
            get { return Fields.Imagepath[this]; }
            set { Fields.Imagepath[this] = value; }
        }

        [DisplayName("Bgpath"), Column("bgpath")]
        public String Bgpath
        {
            get { return Fields.Bgpath[this]; }
            set { Fields.Bgpath[this] = value; }
        }

        [DisplayName("Lowerx"), Column("lowerx")]
        public Int32? Lowerx
        {
            get { return Fields.Lowerx[this]; }
            set { Fields.Lowerx[this] = value; }
        }

        [DisplayName("Lowery"), Column("lowery")]
        public Int32? Lowery
        {
            get { return Fields.Lowery[this]; }
            set { Fields.Lowery[this] = value; }
        }

        [DisplayName("Upperx"), Column("upperx")]
        public Int32? Upperx
        {
            get { return Fields.Upperx[this]; }
            set { Fields.Upperx[this] = value; }
        }

        [DisplayName("Uppery"), Column("uppery")]
        public Int32? Uppery
        {
            get { return Fields.Uppery[this]; }
            set { Fields.Uppery[this] = value; }
        }

        [DisplayName("Possitiontype"), Column("possitiontype")]
        public Int32? Possitiontype
        {
            get { return Fields.Possitiontype[this]; }
            set { Fields.Possitiontype[this] = value; }
        }

        [DisplayName("Formelementname"), Column("formelementname")]
        public String Formelementname
        {
            get { return Fields.Formelementname[this]; }
            set { Fields.Formelementname[this] = value; }
        }

        [DisplayName("Signaturetext"), Column("signaturetext")]
        public String Signaturetext
        {
            get { return Fields.Signaturetext[this]; }
            set { Fields.Signaturetext[this] = value; }
        }

        [DisplayName("Statustext"), Column("statustext")]
        public String Statustext
        {
            get { return Fields.Statustext[this]; }
            set { Fields.Statustext[this] = value; }
        }

        [DisplayName("Signaturelocation"), Column("signaturelocation")]
        public String Signaturelocation
        {
            get { return Fields.Signaturelocation[this]; }
            set { Fields.Signaturelocation[this] = value; }
        }

        [DisplayName("Pagenumber"), Column("pagenumber")]
        public Int32? Pagenumber
        {
            get { return Fields.Pagenumber[this]; }
            set { Fields.Pagenumber[this] = value; }
        }

        [DisplayName("Rendermode"), Column("rendermode")]
        public Int32? Rendermode
        {
            get { return Fields.Rendermode[this]; }
            set { Fields.Rendermode[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Signatureid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Signaturename; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public SignaturesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Signatureid;
            public StringField Signaturename;
            public StreamField Isvisiblesignature;
            public StringField Imagepath;
            public StringField Bgpath;
            public Int32Field Lowerx;
            public Int32Field Lowery;
            public Int32Field Upperx;
            public Int32Field Uppery;
            public Int32Field Possitiontype;
            public StringField Formelementname;
            public StringField Signaturetext;
            public StringField Statustext;
            public StringField Signaturelocation;
            public Int32Field Pagenumber;
            public Int32Field Rendermode;
        }
    }
}
