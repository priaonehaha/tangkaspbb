﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class SignaturesDialog extends Serenity.EntityDialog<SignaturesRow, any> {
        protected getFormKey() { return SignaturesForm.formKey; }
        protected getIdProperty() { return SignaturesRow.idProperty; }
        protected getLocalTextPrefix() { return SignaturesRow.localTextPrefix; }
        protected getNameProperty() { return SignaturesRow.nameProperty; }
        protected getService() { return SignaturesService.baseUrl; }

        protected form = new SignaturesForm(this.idPrefix);

    }
}