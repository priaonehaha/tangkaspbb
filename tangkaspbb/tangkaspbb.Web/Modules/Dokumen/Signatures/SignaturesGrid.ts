﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class SignaturesGrid extends Serenity.EntityGrid<SignaturesRow, any> {
        protected getColumnsKey() { return 'Dokumen.Signatures'; }
        protected getDialogType() { return SignaturesDialog; }
        protected getIdProperty() { return SignaturesRow.idProperty; }
        protected getLocalTextPrefix() { return SignaturesRow.localTextPrefix; }
        protected getService() { return SignaturesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}