﻿
namespace tangkaspbb.Dokumen.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Dokumen.Signatures")]
    [BasedOnRow(typeof(Entities.SignaturesRow), CheckNames = true)]
    public class SignaturesForm
    {
        [Category("General")]
        public String Signaturename { get; set; }
        public Stream Isvisiblesignature { get; set; }
        public String Imagepath { get; set; }
        public String Bgpath { get; set; }
        public Int32 Lowerx { get; set; }
        public Int32 Lowery { get; set; }
        public Int32 Upperx { get; set; }
        public Int32 Uppery { get; set; }
        public Int32 Possitiontype { get; set; }
        public String Formelementname { get; set; }
        public String Signaturetext { get; set; }
        public String Statustext { get; set; }
        public String Signaturelocation { get; set; }
        public Int32 Pagenumber { get; set; }
        public Int32 Rendermode { get; set; }
    }
}