﻿
namespace tangkaspbb.Dokumen.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Dokumen.Certificates")]
    [BasedOnRow(typeof(Entities.CertificatesRow), CheckNames = true)]
    public class CertificatesColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Certificateid { get; set; }
        [EditLink]
        public String Certificatename { get; set; }
        public String Issuedto { get; set; }
        public String Issuedby { get; set; }
        public DateTime Validfrom { get; set; }
        public DateTime Validto { get; set; }
        public Int32 Certificatetype { get; set; }
        public String Serialnumber { get; set; }
        public String Thumbprint { get; set; }
        public String Subjectcn { get; set; }
        public String Subjecto { get; set; }
        public String Subjectl { get; set; }
        public String Subjects { get; set; }
        public String Subjectc { get; set; }
        public Int32 Tsaconfigid { get; set; }
        public Stream Ocspenable { get; set; }
        public String Ocspserver { get; set; }
        public String Keystorepath { get; set; }
        public Int32 Keystoretype { get; set; }
        public Int32 Hashalgorithm { get; set; }
        public String Tsaurl { get; set; }
        public Int32 Tsaauthtype { get; set; }
        public String Authusername { get; set; }
        public String Authpassword { get; set; }
        public String Tsahashalgorithm { get; set; }
    }
}