﻿
namespace tangkaspbb.Dokumen.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Dokumen"), TableName("[public].[certificates]")]
    [DisplayName("Certificates"), InstanceName("Certificates")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class CertificatesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Certificateid"), Column("certificateid"), PrimaryKey]
        public Int32? Certificateid
        {
            get { return Fields.Certificateid[this]; }
            set { Fields.Certificateid[this] = value; }
        }

        [DisplayName("Certificatename"), Column("certificatename"), QuickSearch]
        public String Certificatename
        {
            get { return Fields.Certificatename[this]; }
            set { Fields.Certificatename[this] = value; }
        }

        [DisplayName("Issuedto"), Column("issuedto")]
        public String Issuedto
        {
            get { return Fields.Issuedto[this]; }
            set { Fields.Issuedto[this] = value; }
        }

        [DisplayName("Issuedby"), Column("issuedby")]
        public String Issuedby
        {
            get { return Fields.Issuedby[this]; }
            set { Fields.Issuedby[this] = value; }
        }

        [DisplayName("Validfrom"), Column("validfrom")]
        public DateTime? Validfrom
        {
            get { return Fields.Validfrom[this]; }
            set { Fields.Validfrom[this] = value; }
        }

        [DisplayName("Validto"), Column("validto")]
        public DateTime? Validto
        {
            get { return Fields.Validto[this]; }
            set { Fields.Validto[this] = value; }
        }

        [DisplayName("Certificatetype"), Column("certificatetype")]
        public Int32? Certificatetype
        {
            get { return Fields.Certificatetype[this]; }
            set { Fields.Certificatetype[this] = value; }
        }

        [DisplayName("Serialnumber"), Column("serialnumber")]
        public String Serialnumber
        {
            get { return Fields.Serialnumber[this]; }
            set { Fields.Serialnumber[this] = value; }
        }

        [DisplayName("Thumbprint"), Column("thumbprint")]
        public String Thumbprint
        {
            get { return Fields.Thumbprint[this]; }
            set { Fields.Thumbprint[this] = value; }
        }

        [DisplayName("Subjectcn"), Column("subjectcn")]
        public String Subjectcn
        {
            get { return Fields.Subjectcn[this]; }
            set { Fields.Subjectcn[this] = value; }
        }

        [DisplayName("Subjecto"), Column("subjecto")]
        public String Subjecto
        {
            get { return Fields.Subjecto[this]; }
            set { Fields.Subjecto[this] = value; }
        }

        [DisplayName("Subjectl"), Column("subjectl")]
        public String Subjectl
        {
            get { return Fields.Subjectl[this]; }
            set { Fields.Subjectl[this] = value; }
        }

        [DisplayName("Subjects"), Column("subjects")]
        public String Subjects
        {
            get { return Fields.Subjects[this]; }
            set { Fields.Subjects[this] = value; }
        }

        [DisplayName("Subjectc"), Column("subjectc")]
        public String Subjectc
        {
            get { return Fields.Subjectc[this]; }
            set { Fields.Subjectc[this] = value; }
        }

        [DisplayName("Tsaconfigid"), Column("tsaconfigid")]
        public Int32? Tsaconfigid
        {
            get { return Fields.Tsaconfigid[this]; }
            set { Fields.Tsaconfigid[this] = value; }
        }

        [DisplayName("Ocspenable"), Column("ocspenable")]
        public Stream Ocspenable
        {
            get { return Fields.Ocspenable[this]; }
            set { Fields.Ocspenable[this] = value; }
        }

        [DisplayName("Ocspserver"), Column("ocspserver")]
        public String Ocspserver
        {
            get { return Fields.Ocspserver[this]; }
            set { Fields.Ocspserver[this] = value; }
        }

        [DisplayName("Keystorepath"), Column("keystorepath")]
        public String Keystorepath
        {
            get { return Fields.Keystorepath[this]; }
            set { Fields.Keystorepath[this] = value; }
        }

        [DisplayName("Keystoretype"), Column("keystoretype")]
        public Int32? Keystoretype
        {
            get { return Fields.Keystoretype[this]; }
            set { Fields.Keystoretype[this] = value; }
        }

        [DisplayName("Hashalgorithm"), Column("hashalgorithm")]
        public Int32? Hashalgorithm
        {
            get { return Fields.Hashalgorithm[this]; }
            set { Fields.Hashalgorithm[this] = value; }
        }

        [DisplayName("Tsaurl"), Column("tsaurl")]
        public String Tsaurl
        {
            get { return Fields.Tsaurl[this]; }
            set { Fields.Tsaurl[this] = value; }
        }

        [DisplayName("Tsaauthtype"), Column("tsaauthtype")]
        public Int32? Tsaauthtype
        {
            get { return Fields.Tsaauthtype[this]; }
            set { Fields.Tsaauthtype[this] = value; }
        }

        [DisplayName("Authusername"), Column("authusername"), Size(100)]
        public String Authusername
        {
            get { return Fields.Authusername[this]; }
            set { Fields.Authusername[this] = value; }
        }

        [DisplayName("Authpassword"), Column("authpassword")]
        public String Authpassword
        {
            get { return Fields.Authpassword[this]; }
            set { Fields.Authpassword[this] = value; }
        }

        [DisplayName("Tsahashalgorithm"), Column("tsahashalgorithm")]
        public String Tsahashalgorithm
        {
            get { return Fields.Tsahashalgorithm[this]; }
            set { Fields.Tsahashalgorithm[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Certificateid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Certificatename; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public CertificatesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Certificateid;
            public StringField Certificatename;
            public StringField Issuedto;
            public StringField Issuedby;
            public DateTimeField Validfrom;
            public DateTimeField Validto;
            public Int32Field Certificatetype;
            public StringField Serialnumber;
            public StringField Thumbprint;
            public StringField Subjectcn;
            public StringField Subjecto;
            public StringField Subjectl;
            public StringField Subjects;
            public StringField Subjectc;
            public Int32Field Tsaconfigid;
            public StreamField Ocspenable;
            public StringField Ocspserver;
            public StringField Keystorepath;
            public Int32Field Keystoretype;
            public Int32Field Hashalgorithm;
            public StringField Tsaurl;
            public Int32Field Tsaauthtype;
            public StringField Authusername;
            public StringField Authpassword;
            public StringField Tsahashalgorithm;
        }
    }
}
