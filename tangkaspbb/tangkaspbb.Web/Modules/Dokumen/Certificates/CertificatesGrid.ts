﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class CertificatesGrid extends Serenity.EntityGrid<CertificatesRow, any> {
        protected getColumnsKey() { return 'Dokumen.Certificates'; }
        protected getDialogType() { return CertificatesDialog; }
        protected getIdProperty() { return CertificatesRow.idProperty; }
        protected getLocalTextPrefix() { return CertificatesRow.localTextPrefix; }
        protected getService() { return CertificatesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}