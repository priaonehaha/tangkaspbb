﻿
namespace tangkaspbb.Dokumen.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.CertificatesRow))]
    public class CertificatesController : Controller
    {
        [Route("Dokumen/Certificates")]
        public ActionResult Index()
        {
            return View("~/Modules/Dokumen/Certificates/CertificatesIndex.cshtml");
        }
    }
}