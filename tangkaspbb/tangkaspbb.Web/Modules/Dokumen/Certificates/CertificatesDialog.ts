﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class CertificatesDialog extends Serenity.EntityDialog<CertificatesRow, any> {
        protected getFormKey() { return CertificatesForm.formKey; }
        protected getIdProperty() { return CertificatesRow.idProperty; }
        protected getLocalTextPrefix() { return CertificatesRow.localTextPrefix; }
        protected getNameProperty() { return CertificatesRow.nameProperty; }
        protected getService() { return CertificatesService.baseUrl; }

        protected form = new CertificatesForm(this.idPrefix);

    }
}