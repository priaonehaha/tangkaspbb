﻿
namespace tangkaspbb.Dokumen.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Dokumen"), TableName("[public].[documenttypes]")]
    [DisplayName("Documenttypes"), InstanceName("Documenttypes")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class DocumenttypesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Documenttypeid"), Column("documenttypeid"), PrimaryKey]
        public Int32? Documenttypeid
        {
            get { return Fields.Documenttypeid[this]; }
            set { Fields.Documenttypeid[this] = value; }
        }

        [DisplayName("Documenttypename"), Column("documenttypename"), Size(100), QuickSearch]
        public String Documenttypename
        {
            get { return Fields.Documenttypename[this]; }
            set { Fields.Documenttypename[this] = value; }
        }

        [DisplayName("Outputsuffix"), Column("outputsuffix")]
        public String Outputsuffix
        {
            get { return Fields.Outputsuffix[this]; }
            set { Fields.Outputsuffix[this] = value; }
        }

        [DisplayName("Outputprefix"), Column("outputprefix")]
        public String Outputprefix
        {
            get { return Fields.Outputprefix[this]; }
            set { Fields.Outputprefix[this] = value; }
        }

        [DisplayName("Isdisableassembly"), Column("isdisableassembly")]
        public Stream Isdisableassembly
        {
            get { return Fields.Isdisableassembly[this]; }
            set { Fields.Isdisableassembly[this] = value; }
        }

        [DisplayName("Isdisablecopy"), Column("isdisablecopy")]
        public Stream Isdisablecopy
        {
            get { return Fields.Isdisablecopy[this]; }
            set { Fields.Isdisablecopy[this] = value; }
        }

        [DisplayName("Isdisablefill"), Column("isdisablefill")]
        public Stream Isdisablefill
        {
            get { return Fields.Isdisablefill[this]; }
            set { Fields.Isdisablefill[this] = value; }
        }

        [DisplayName("Isdisablemodifyannotations"), Column("isdisablemodifyannotations")]
        public Stream Isdisablemodifyannotations
        {
            get { return Fields.Isdisablemodifyannotations[this]; }
            set { Fields.Isdisablemodifyannotations[this] = value; }
        }

        [DisplayName("Isdisablemodifycontent"), Column("isdisablemodifycontent")]
        public Stream Isdisablemodifycontent
        {
            get { return Fields.Isdisablemodifycontent[this]; }
            set { Fields.Isdisablemodifycontent[this] = value; }
        }

        [DisplayName("Isdisablescreenreaders"), Column("isdisablescreenreaders")]
        public Stream Isdisablescreenreaders
        {
            get { return Fields.Isdisablescreenreaders[this]; }
            set { Fields.Isdisablescreenreaders[this] = value; }
        }

        [DisplayName("Certificationlevel"), Column("certificationlevel")]
        public Int32? Certificationlevel
        {
            get { return Fields.Certificationlevel[this]; }
            set { Fields.Certificationlevel[this] = value; }
        }

        [DisplayName("Officialid"), Column("officialid"), ForeignKey("[public].[officials]", "officialid"), LeftJoin("jOfficialid"), TextualField("OfficialidOfficialname")]
        public Int32? Officialid
        {
            get { return Fields.Officialid[this]; }
            set { Fields.Officialid[this] = value; }
        }

        [DisplayName("Printright"), Column("printright")]
        public Int32? Printright
        {
            get { return Fields.Printright[this]; }
            set { Fields.Printright[this] = value; }
        }

        [DisplayName("Certificateid"), Column("certificateid"), ForeignKey("[public].[certificates]", "certificateid"), LeftJoin("jCertificateid"), TextualField("CertificateidCertificatename")]
        public Int32? Certificateid
        {
            get { return Fields.Certificateid[this]; }
            set { Fields.Certificateid[this] = value; }
        }

        [DisplayName("Signatureid"), Column("signatureid"), ForeignKey("[public].[signatures]", "signatureid"), LeftJoin("jSignatureid"), TextualField("SignatureidSignaturename")]
        public Int32? Signatureid
        {
            get { return Fields.Signatureid[this]; }
            set { Fields.Signatureid[this] = value; }
        }

        [DisplayName("Isencrypted"), Column("isencrypted")]
        public Stream Isencrypted
        {
            get { return Fields.Isencrypted[this]; }
            set { Fields.Isencrypted[this] = value; }
        }

        [DisplayName("Userpassword"), Column("userpassword")]
        public String Userpassword
        {
            get { return Fields.Userpassword[this]; }
            set { Fields.Userpassword[this] = value; }
        }

        [DisplayName("Ownerpassword"), Column("ownerpassword")]
        public String Ownerpassword
        {
            get { return Fields.Ownerpassword[this]; }
            set { Fields.Ownerpassword[this] = value; }
        }

        [DisplayName("Institutionid"), Column("institutionid"), ForeignKey("[public].[institutions]", "institutionid"), LeftJoin("jInstitutionid"), TextualField("InstitutionidInstitutionname")]
        public Int32? Institutionid
        {
            get { return Fields.Institutionid[this]; }
            set { Fields.Institutionid[this] = value; }
        }

        [DisplayName("Officialid Officialname"), Expression("jOfficialid.[officialname]")]
        public String OfficialidOfficialname
        {
            get { return Fields.OfficialidOfficialname[this]; }
            set { Fields.OfficialidOfficialname[this] = value; }
        }

        [DisplayName("Officialid Institutionid"), Expression("jOfficialid.[institutionid]")]
        public Int32? OfficialidInstitutionid
        {
            get { return Fields.OfficialidInstitutionid[this]; }
            set { Fields.OfficialidInstitutionid[this] = value; }
        }

        [DisplayName("Officialid Servedfrom"), Expression("jOfficialid.[servedfrom]")]
        public DateTime? OfficialidServedfrom
        {
            get { return Fields.OfficialidServedfrom[this]; }
            set { Fields.OfficialidServedfrom[this] = value; }
        }

        [DisplayName("Officialid Servedto"), Expression("jOfficialid.[servedto]")]
        public DateTime? OfficialidServedto
        {
            get { return Fields.OfficialidServedto[this]; }
            set { Fields.OfficialidServedto[this] = value; }
        }

        [DisplayName("Officialid Certificateid"), Expression("jOfficialid.[certificateid]")]
        public Int32? OfficialidCertificateid
        {
            get { return Fields.OfficialidCertificateid[this]; }
            set { Fields.OfficialidCertificateid[this] = value; }
        }

        [DisplayName("Officialid Positionid"), Expression("jOfficialid.[positionid]")]
        public Int32? OfficialidPositionid
        {
            get { return Fields.OfficialidPositionid[this]; }
            set { Fields.OfficialidPositionid[this] = value; }
        }

        [DisplayName("Officialid Signatureid"), Expression("jOfficialid.[signatureid]")]
        public Int32? OfficialidSignatureid
        {
            get { return Fields.OfficialidSignatureid[this]; }
            set { Fields.OfficialidSignatureid[this] = value; }
        }

        [DisplayName("Certificateid Certificatename"), Expression("jCertificateid.[certificatename]")]
        public String CertificateidCertificatename
        {
            get { return Fields.CertificateidCertificatename[this]; }
            set { Fields.CertificateidCertificatename[this] = value; }
        }

        [DisplayName("Certificateid Issuedto"), Expression("jCertificateid.[issuedto]")]
        public String CertificateidIssuedto
        {
            get { return Fields.CertificateidIssuedto[this]; }
            set { Fields.CertificateidIssuedto[this] = value; }
        }

        [DisplayName("Certificateid Issuedby"), Expression("jCertificateid.[issuedby]")]
        public String CertificateidIssuedby
        {
            get { return Fields.CertificateidIssuedby[this]; }
            set { Fields.CertificateidIssuedby[this] = value; }
        }

        [DisplayName("Certificateid Validfrom"), Expression("jCertificateid.[validfrom]")]
        public DateTime? CertificateidValidfrom
        {
            get { return Fields.CertificateidValidfrom[this]; }
            set { Fields.CertificateidValidfrom[this] = value; }
        }

        [DisplayName("Certificateid Validto"), Expression("jCertificateid.[validto]")]
        public DateTime? CertificateidValidto
        {
            get { return Fields.CertificateidValidto[this]; }
            set { Fields.CertificateidValidto[this] = value; }
        }

        [DisplayName("Certificateid Certificatetype"), Expression("jCertificateid.[certificatetype]")]
        public Int32? CertificateidCertificatetype
        {
            get { return Fields.CertificateidCertificatetype[this]; }
            set { Fields.CertificateidCertificatetype[this] = value; }
        }

        [DisplayName("Certificateid Serialnumber"), Expression("jCertificateid.[serialnumber]")]
        public String CertificateidSerialnumber
        {
            get { return Fields.CertificateidSerialnumber[this]; }
            set { Fields.CertificateidSerialnumber[this] = value; }
        }

        [DisplayName("Certificateid Thumbprint"), Expression("jCertificateid.[thumbprint]")]
        public String CertificateidThumbprint
        {
            get { return Fields.CertificateidThumbprint[this]; }
            set { Fields.CertificateidThumbprint[this] = value; }
        }

        [DisplayName("Certificateid Subjectcn"), Expression("jCertificateid.[subjectcn]")]
        public String CertificateidSubjectcn
        {
            get { return Fields.CertificateidSubjectcn[this]; }
            set { Fields.CertificateidSubjectcn[this] = value; }
        }

        [DisplayName("Certificateid Subjecto"), Expression("jCertificateid.[subjecto]")]
        public String CertificateidSubjecto
        {
            get { return Fields.CertificateidSubjecto[this]; }
            set { Fields.CertificateidSubjecto[this] = value; }
        }

        [DisplayName("Certificateid Subjectl"), Expression("jCertificateid.[subjectl]")]
        public String CertificateidSubjectl
        {
            get { return Fields.CertificateidSubjectl[this]; }
            set { Fields.CertificateidSubjectl[this] = value; }
        }

        [DisplayName("Certificateid Subjects"), Expression("jCertificateid.[subjects]")]
        public String CertificateidSubjects
        {
            get { return Fields.CertificateidSubjects[this]; }
            set { Fields.CertificateidSubjects[this] = value; }
        }

        [DisplayName("Certificateid Subjectc"), Expression("jCertificateid.[subjectc]")]
        public String CertificateidSubjectc
        {
            get { return Fields.CertificateidSubjectc[this]; }
            set { Fields.CertificateidSubjectc[this] = value; }
        }

        [DisplayName("Certificateid Tsaconfigid"), Expression("jCertificateid.[tsaconfigid]")]
        public Int32? CertificateidTsaconfigid
        {
            get { return Fields.CertificateidTsaconfigid[this]; }
            set { Fields.CertificateidTsaconfigid[this] = value; }
        }

        [DisplayName("Certificateid Ocspenable"), Expression("jCertificateid.[ocspenable]")]
        public Stream CertificateidOcspenable
        {
            get { return Fields.CertificateidOcspenable[this]; }
            set { Fields.CertificateidOcspenable[this] = value; }
        }

        [DisplayName("Certificateid Ocspserver"), Expression("jCertificateid.[ocspserver]")]
        public String CertificateidOcspserver
        {
            get { return Fields.CertificateidOcspserver[this]; }
            set { Fields.CertificateidOcspserver[this] = value; }
        }

        [DisplayName("Certificateid Keystorepath"), Expression("jCertificateid.[keystorepath]")]
        public String CertificateidKeystorepath
        {
            get { return Fields.CertificateidKeystorepath[this]; }
            set { Fields.CertificateidKeystorepath[this] = value; }
        }

        [DisplayName("Certificateid Keystoretype"), Expression("jCertificateid.[keystoretype]")]
        public Int32? CertificateidKeystoretype
        {
            get { return Fields.CertificateidKeystoretype[this]; }
            set { Fields.CertificateidKeystoretype[this] = value; }
        }

        [DisplayName("Certificateid Hashalgorithm"), Expression("jCertificateid.[hashalgorithm]")]
        public Int32? CertificateidHashalgorithm
        {
            get { return Fields.CertificateidHashalgorithm[this]; }
            set { Fields.CertificateidHashalgorithm[this] = value; }
        }

        [DisplayName("Certificateid Tsaurl"), Expression("jCertificateid.[tsaurl]")]
        public String CertificateidTsaurl
        {
            get { return Fields.CertificateidTsaurl[this]; }
            set { Fields.CertificateidTsaurl[this] = value; }
        }

        [DisplayName("Certificateid Tsaauthtype"), Expression("jCertificateid.[tsaauthtype]")]
        public Int32? CertificateidTsaauthtype
        {
            get { return Fields.CertificateidTsaauthtype[this]; }
            set { Fields.CertificateidTsaauthtype[this] = value; }
        }

        [DisplayName("Certificateid Authusername"), Expression("jCertificateid.[authusername]")]
        public String CertificateidAuthusername
        {
            get { return Fields.CertificateidAuthusername[this]; }
            set { Fields.CertificateidAuthusername[this] = value; }
        }

        [DisplayName("Certificateid Authpassword"), Expression("jCertificateid.[authpassword]")]
        public String CertificateidAuthpassword
        {
            get { return Fields.CertificateidAuthpassword[this]; }
            set { Fields.CertificateidAuthpassword[this] = value; }
        }

        [DisplayName("Certificateid Tsahashalgorithm"), Expression("jCertificateid.[tsahashalgorithm]")]
        public String CertificateidTsahashalgorithm
        {
            get { return Fields.CertificateidTsahashalgorithm[this]; }
            set { Fields.CertificateidTsahashalgorithm[this] = value; }
        }

        [DisplayName("Signatureid Signaturename"), Expression("jSignatureid.[signaturename]")]
        public String SignatureidSignaturename
        {
            get { return Fields.SignatureidSignaturename[this]; }
            set { Fields.SignatureidSignaturename[this] = value; }
        }

        [DisplayName("Signatureid Isvisiblesignature"), Expression("jSignatureid.[isvisiblesignature]")]
        public Stream SignatureidIsvisiblesignature
        {
            get { return Fields.SignatureidIsvisiblesignature[this]; }
            set { Fields.SignatureidIsvisiblesignature[this] = value; }
        }

        [DisplayName("Signatureid Imagepath"), Expression("jSignatureid.[imagepath]")]
        public String SignatureidImagepath
        {
            get { return Fields.SignatureidImagepath[this]; }
            set { Fields.SignatureidImagepath[this] = value; }
        }

        [DisplayName("Signatureid Bgpath"), Expression("jSignatureid.[bgpath]")]
        public String SignatureidBgpath
        {
            get { return Fields.SignatureidBgpath[this]; }
            set { Fields.SignatureidBgpath[this] = value; }
        }

        [DisplayName("Signatureid Lowerx"), Expression("jSignatureid.[lowerx]")]
        public Int32? SignatureidLowerx
        {
            get { return Fields.SignatureidLowerx[this]; }
            set { Fields.SignatureidLowerx[this] = value; }
        }

        [DisplayName("Signatureid Lowery"), Expression("jSignatureid.[lowery]")]
        public Int32? SignatureidLowery
        {
            get { return Fields.SignatureidLowery[this]; }
            set { Fields.SignatureidLowery[this] = value; }
        }

        [DisplayName("Signatureid Upperx"), Expression("jSignatureid.[upperx]")]
        public Int32? SignatureidUpperx
        {
            get { return Fields.SignatureidUpperx[this]; }
            set { Fields.SignatureidUpperx[this] = value; }
        }

        [DisplayName("Signatureid Uppery"), Expression("jSignatureid.[uppery]")]
        public Int32? SignatureidUppery
        {
            get { return Fields.SignatureidUppery[this]; }
            set { Fields.SignatureidUppery[this] = value; }
        }

        [DisplayName("Signatureid Possitiontype"), Expression("jSignatureid.[possitiontype]")]
        public Int32? SignatureidPossitiontype
        {
            get { return Fields.SignatureidPossitiontype[this]; }
            set { Fields.SignatureidPossitiontype[this] = value; }
        }

        [DisplayName("Signatureid Formelementname"), Expression("jSignatureid.[formelementname]")]
        public String SignatureidFormelementname
        {
            get { return Fields.SignatureidFormelementname[this]; }
            set { Fields.SignatureidFormelementname[this] = value; }
        }

        [DisplayName("Signatureid Signaturetext"), Expression("jSignatureid.[signaturetext]")]
        public String SignatureidSignaturetext
        {
            get { return Fields.SignatureidSignaturetext[this]; }
            set { Fields.SignatureidSignaturetext[this] = value; }
        }

        [DisplayName("Signatureid Statustext"), Expression("jSignatureid.[statustext]")]
        public String SignatureidStatustext
        {
            get { return Fields.SignatureidStatustext[this]; }
            set { Fields.SignatureidStatustext[this] = value; }
        }

        [DisplayName("Signatureid Signaturelocation"), Expression("jSignatureid.[signaturelocation]")]
        public String SignatureidSignaturelocation
        {
            get { return Fields.SignatureidSignaturelocation[this]; }
            set { Fields.SignatureidSignaturelocation[this] = value; }
        }

        [DisplayName("Signatureid Pagenumber"), Expression("jSignatureid.[pagenumber]")]
        public Int32? SignatureidPagenumber
        {
            get { return Fields.SignatureidPagenumber[this]; }
            set { Fields.SignatureidPagenumber[this] = value; }
        }

        [DisplayName("Signatureid Rendermode"), Expression("jSignatureid.[rendermode]")]
        public Int32? SignatureidRendermode
        {
            get { return Fields.SignatureidRendermode[this]; }
            set { Fields.SignatureidRendermode[this] = value; }
        }

        [DisplayName("Institutionid Institutionname"), Expression("jInstitutionid.[institutionname]")]
        public String InstitutionidInstitutionname
        {
            get { return Fields.InstitutionidInstitutionname[this]; }
            set { Fields.InstitutionidInstitutionname[this] = value; }
        }

        [DisplayName("Institutionid Level"), Expression("jInstitutionid.[level]")]
        public Int32? InstitutionidLevel
        {
            get { return Fields.InstitutionidLevel[this]; }
            set { Fields.InstitutionidLevel[this] = value; }
        }

        [DisplayName("Institutionid Super"), Expression("jInstitutionid.[super]")]
        public Int32? InstitutionidSuper
        {
            get { return Fields.InstitutionidSuper[this]; }
            set { Fields.InstitutionidSuper[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Documenttypeid; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Documenttypename; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public DocumenttypesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Documenttypeid;
            public StringField Documenttypename;
            public StringField Outputsuffix;
            public StringField Outputprefix;
            public StreamField Isdisableassembly;
            public StreamField Isdisablecopy;
            public StreamField Isdisablefill;
            public StreamField Isdisablemodifyannotations;
            public StreamField Isdisablemodifycontent;
            public StreamField Isdisablescreenreaders;
            public Int32Field Certificationlevel;
            public Int32Field Officialid;
            public Int32Field Printright;
            public Int32Field Certificateid;
            public Int32Field Signatureid;
            public StreamField Isencrypted;
            public StringField Userpassword;
            public StringField Ownerpassword;
            public Int32Field Institutionid;

            public StringField OfficialidOfficialname;
            public Int32Field OfficialidInstitutionid;
            public DateTimeField OfficialidServedfrom;
            public DateTimeField OfficialidServedto;
            public Int32Field OfficialidCertificateid;
            public Int32Field OfficialidPositionid;
            public Int32Field OfficialidSignatureid;

            public StringField CertificateidCertificatename;
            public StringField CertificateidIssuedto;
            public StringField CertificateidIssuedby;
            public DateTimeField CertificateidValidfrom;
            public DateTimeField CertificateidValidto;
            public Int32Field CertificateidCertificatetype;
            public StringField CertificateidSerialnumber;
            public StringField CertificateidThumbprint;
            public StringField CertificateidSubjectcn;
            public StringField CertificateidSubjecto;
            public StringField CertificateidSubjectl;
            public StringField CertificateidSubjects;
            public StringField CertificateidSubjectc;
            public Int32Field CertificateidTsaconfigid;
            public StreamField CertificateidOcspenable;
            public StringField CertificateidOcspserver;
            public StringField CertificateidKeystorepath;
            public Int32Field CertificateidKeystoretype;
            public Int32Field CertificateidHashalgorithm;
            public StringField CertificateidTsaurl;
            public Int32Field CertificateidTsaauthtype;
            public StringField CertificateidAuthusername;
            public StringField CertificateidAuthpassword;
            public StringField CertificateidTsahashalgorithm;

            public StringField SignatureidSignaturename;
            public StreamField SignatureidIsvisiblesignature;
            public StringField SignatureidImagepath;
            public StringField SignatureidBgpath;
            public Int32Field SignatureidLowerx;
            public Int32Field SignatureidLowery;
            public Int32Field SignatureidUpperx;
            public Int32Field SignatureidUppery;
            public Int32Field SignatureidPossitiontype;
            public StringField SignatureidFormelementname;
            public StringField SignatureidSignaturetext;
            public StringField SignatureidStatustext;
            public StringField SignatureidSignaturelocation;
            public Int32Field SignatureidPagenumber;
            public Int32Field SignatureidRendermode;

            public StringField InstitutionidInstitutionname;
            public Int32Field InstitutionidLevel;
            public Int32Field InstitutionidSuper;
        }
    }
}
