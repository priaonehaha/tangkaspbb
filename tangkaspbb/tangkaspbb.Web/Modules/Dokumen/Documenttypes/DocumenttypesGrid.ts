﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class DocumenttypesGrid extends Serenity.EntityGrid<DocumenttypesRow, any> {
        protected getColumnsKey() { return 'Dokumen.Documenttypes'; }
        protected getDialogType() { return DocumenttypesDialog; }
        protected getIdProperty() { return DocumenttypesRow.idProperty; }
        protected getLocalTextPrefix() { return DocumenttypesRow.localTextPrefix; }
        protected getService() { return DocumenttypesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}