﻿
namespace tangkaspbb.Dokumen.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Dokumen.Documenttypes")]
    [BasedOnRow(typeof(Entities.DocumenttypesRow), CheckNames = true)]
    public class DocumenttypesForm
    {
        public String Documenttypename { get; set; }
        public String Outputsuffix { get; set; }
        public String Outputprefix { get; set; }
        public Stream Isdisableassembly { get; set; }
        public Stream Isdisablecopy { get; set; }
        public Stream Isdisablefill { get; set; }
        public Stream Isdisablemodifyannotations { get; set; }
        public Stream Isdisablemodifycontent { get; set; }
        public Stream Isdisablescreenreaders { get; set; }
        public Int32 Certificationlevel { get; set; }
        public Int32 Officialid { get; set; }
        public Int32 Printright { get; set; }
        public Int32 Certificateid { get; set; }
        public Int32 Signatureid { get; set; }
        public Stream Isencrypted { get; set; }
        public String Userpassword { get; set; }
        public String Ownerpassword { get; set; }
        public Int32 Institutionid { get; set; }
    }
}