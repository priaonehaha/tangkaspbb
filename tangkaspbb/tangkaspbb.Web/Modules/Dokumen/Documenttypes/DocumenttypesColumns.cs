﻿
namespace tangkaspbb.Dokumen.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Dokumen.Documenttypes")]
    [BasedOnRow(typeof(Entities.DocumenttypesRow), CheckNames = true)]
    public class DocumenttypesColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Documenttypeid { get; set; }
        [EditLink]
        public String Documenttypename { get; set; }
        public String Outputsuffix { get; set; }
        public String Outputprefix { get; set; }
        public Stream Isdisableassembly { get; set; }
        public Stream Isdisablecopy { get; set; }
        public Stream Isdisablefill { get; set; }
        public Stream Isdisablemodifyannotations { get; set; }
        public Stream Isdisablemodifycontent { get; set; }
        public Stream Isdisablescreenreaders { get; set; }
        public Int32 Certificationlevel { get; set; }
        public String OfficialidOfficialname { get; set; }
        public Int32 Printright { get; set; }
        public String CertificateidCertificatename { get; set; }
        public String SignatureidSignaturename { get; set; }
        public Stream Isencrypted { get; set; }
        public String Userpassword { get; set; }
        public String Ownerpassword { get; set; }
        public String InstitutionidInstitutionname { get; set; }
    }
}