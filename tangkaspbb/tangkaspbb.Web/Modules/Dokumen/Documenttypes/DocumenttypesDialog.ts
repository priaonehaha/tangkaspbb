﻿
namespace tangkaspbb.Dokumen {

    @Serenity.Decorators.registerClass()
    export class DocumenttypesDialog extends Serenity.EntityDialog<DocumenttypesRow, any> {
        protected getFormKey() { return DocumenttypesForm.formKey; }
        protected getIdProperty() { return DocumenttypesRow.idProperty; }
        protected getLocalTextPrefix() { return DocumenttypesRow.localTextPrefix; }
        protected getNameProperty() { return DocumenttypesRow.nameProperty; }
        protected getService() { return DocumenttypesService.baseUrl; }

        protected form = new DocumenttypesForm(this.idPrefix);

    }
}