﻿
namespace tangkaspbb.Dokumen.Pages
{
    using Serenity;
    using Serenity.Web;
    using Microsoft.AspNetCore.Mvc;

    [PageAuthorize(typeof(Entities.DocumenttypesRow))]
    public class DocumenttypesController : Controller
    {
        [Route("Dokumen/Documenttypes")]
        public ActionResult Index()
        {
            return View("~/Modules/Dokumen/Documenttypes/DocumenttypesIndex.cshtml");
        }
    }
}