﻿using Serenity.Services;

namespace tangkaspbb.Northwind
{
    public class OrderListRequest : ListRequest
    {
        public int? ProductID { get; set; }
    }
}