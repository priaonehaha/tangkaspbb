﻿namespace tangkaspbb.Organisasi {
    export interface OfficialsForm {
        Officialname: Serenity.StringEditor;
        Institutionid: Serenity.IntegerEditor;
        Servedfrom: Serenity.DateEditor;
        Servedto: Serenity.DateEditor;
        Certificateid: Serenity.IntegerEditor;
        Positionid: Serenity.IntegerEditor;
        Signatureid: Serenity.IntegerEditor;
    }

    export class OfficialsForm extends Serenity.PrefixedContext {
        static formKey = 'Organisasi.Officials';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!OfficialsForm.init)  {
                OfficialsForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.IntegerEditor;
                var w2 = s.DateEditor;

                Q.initFormType(OfficialsForm, [
                    'Officialname', w0,
                    'Institutionid', w1,
                    'Servedfrom', w2,
                    'Servedto', w2,
                    'Certificateid', w1,
                    'Positionid', w1,
                    'Signatureid', w1
                ]);
            }
        }
    }
}
