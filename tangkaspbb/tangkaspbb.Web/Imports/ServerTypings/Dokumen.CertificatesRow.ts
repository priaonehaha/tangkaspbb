﻿namespace tangkaspbb.Dokumen {
    export interface CertificatesRow {
        Certificateid?: number;
        Certificatename?: string;
        Issuedto?: string;
        Issuedby?: string;
        Validfrom?: string;
        Validto?: string;
        Certificatetype?: number;
        Serialnumber?: string;
        Thumbprint?: string;
        Subjectcn?: string;
        Subjecto?: string;
        Subjectl?: string;
        Subjects?: string;
        Subjectc?: string;
        Tsaconfigid?: number;
        Ocspenable?: number[];
        Ocspserver?: string;
        Keystorepath?: string;
        Keystoretype?: number;
        Hashalgorithm?: number;
        Tsaurl?: string;
        Tsaauthtype?: number;
        Authusername?: string;
        Authpassword?: string;
        Tsahashalgorithm?: string;
    }

    export namespace CertificatesRow {
        export const idProperty = 'Certificateid';
        export const nameProperty = 'Certificatename';
        export const localTextPrefix = 'Dokumen.Certificates';

        export declare const enum Fields {
            Certificateid = "Certificateid",
            Certificatename = "Certificatename",
            Issuedto = "Issuedto",
            Issuedby = "Issuedby",
            Validfrom = "Validfrom",
            Validto = "Validto",
            Certificatetype = "Certificatetype",
            Serialnumber = "Serialnumber",
            Thumbprint = "Thumbprint",
            Subjectcn = "Subjectcn",
            Subjecto = "Subjecto",
            Subjectl = "Subjectl",
            Subjects = "Subjects",
            Subjectc = "Subjectc",
            Tsaconfigid = "Tsaconfigid",
            Ocspenable = "Ocspenable",
            Ocspserver = "Ocspserver",
            Keystorepath = "Keystorepath",
            Keystoretype = "Keystoretype",
            Hashalgorithm = "Hashalgorithm",
            Tsaurl = "Tsaurl",
            Tsaauthtype = "Tsaauthtype",
            Authusername = "Authusername",
            Authpassword = "Authpassword",
            Tsahashalgorithm = "Tsahashalgorithm"
        }
    }
}
