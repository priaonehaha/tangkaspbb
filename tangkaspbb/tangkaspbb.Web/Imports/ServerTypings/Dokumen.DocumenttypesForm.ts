﻿namespace tangkaspbb.Dokumen {
    export interface DocumenttypesForm {
        Documenttypename: Serenity.StringEditor;
        Outputsuffix: Serenity.StringEditor;
        Outputprefix: Serenity.StringEditor;
        Isdisableassembly: Serenity.StringEditor;
        Isdisablecopy: Serenity.StringEditor;
        Isdisablefill: Serenity.StringEditor;
        Isdisablemodifyannotations: Serenity.StringEditor;
        Isdisablemodifycontent: Serenity.StringEditor;
        Isdisablescreenreaders: Serenity.StringEditor;
        Certificationlevel: Serenity.IntegerEditor;
        Officialid: Serenity.IntegerEditor;
        Printright: Serenity.IntegerEditor;
        Certificateid: Serenity.IntegerEditor;
        Signatureid: Serenity.IntegerEditor;
        Isencrypted: Serenity.StringEditor;
        Userpassword: Serenity.StringEditor;
        Ownerpassword: Serenity.StringEditor;
        Institutionid: Serenity.IntegerEditor;
    }

    export class DocumenttypesForm extends Serenity.PrefixedContext {
        static formKey = 'Dokumen.Documenttypes';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!DocumenttypesForm.init)  {
                DocumenttypesForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.IntegerEditor;

                Q.initFormType(DocumenttypesForm, [
                    'Documenttypename', w0,
                    'Outputsuffix', w0,
                    'Outputprefix', w0,
                    'Isdisableassembly', w0,
                    'Isdisablecopy', w0,
                    'Isdisablefill', w0,
                    'Isdisablemodifyannotations', w0,
                    'Isdisablemodifycontent', w0,
                    'Isdisablescreenreaders', w0,
                    'Certificationlevel', w1,
                    'Officialid', w1,
                    'Printright', w1,
                    'Certificateid', w1,
                    'Signatureid', w1,
                    'Isencrypted', w0,
                    'Userpassword', w0,
                    'Ownerpassword', w0,
                    'Institutionid', w1
                ]);
            }
        }
    }
}
