﻿namespace tangkaspbb.Organisasi {
    export interface PositionsRow {
        Positionid?: number;
        Positionname?: string;
        Institutionid?: number;
        Level?: number;
        Super?: number;
        InstitutionidInstitutionname?: string;
        InstitutionidLevel?: number;
        InstitutionidSuper?: number;
        SuperPositionname?: string;
        SuperInstitutionid?: number;
        SuperLevel?: number;
        Super1?: number;
    }

    export namespace PositionsRow {
        export const idProperty = 'Positionid';
        export const nameProperty = 'Positionname';
        export const localTextPrefix = 'Organisasi.Positions';

        export declare const enum Fields {
            Positionid = "Positionid",
            Positionname = "Positionname",
            Institutionid = "Institutionid",
            Level = "Level",
            Super = "Super",
            InstitutionidInstitutionname = "InstitutionidInstitutionname",
            InstitutionidLevel = "InstitutionidLevel",
            InstitutionidSuper = "InstitutionidSuper",
            SuperPositionname = "SuperPositionname",
            SuperInstitutionid = "SuperInstitutionid",
            SuperLevel = "SuperLevel",
            Super1 = "Super1"
        }
    }
}
