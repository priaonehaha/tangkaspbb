﻿namespace tangkaspbb.Dokumen {
    export interface SignaturesRow {
        Signatureid?: number;
        Signaturename?: string;
        Isvisiblesignature?: number[];
        Imagepath?: string;
        Bgpath?: string;
        Lowerx?: number;
        Lowery?: number;
        Upperx?: number;
        Uppery?: number;
        Possitiontype?: number;
        Formelementname?: string;
        Signaturetext?: string;
        Statustext?: string;
        Signaturelocation?: string;
        Pagenumber?: number;
        Rendermode?: number;
    }

    export namespace SignaturesRow {
        export const idProperty = 'Signatureid';
        export const nameProperty = 'Signaturename';
        export const localTextPrefix = 'Dokumen.Signatures';

        export declare const enum Fields {
            Signatureid = "Signatureid",
            Signaturename = "Signaturename",
            Isvisiblesignature = "Isvisiblesignature",
            Imagepath = "Imagepath",
            Bgpath = "Bgpath",
            Lowerx = "Lowerx",
            Lowery = "Lowery",
            Upperx = "Upperx",
            Uppery = "Uppery",
            Possitiontype = "Possitiontype",
            Formelementname = "Formelementname",
            Signaturetext = "Signaturetext",
            Statustext = "Statustext",
            Signaturelocation = "Signaturelocation",
            Pagenumber = "Pagenumber",
            Rendermode = "Rendermode"
        }
    }
}
