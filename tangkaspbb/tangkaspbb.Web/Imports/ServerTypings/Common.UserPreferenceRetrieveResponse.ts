﻿namespace tangkaspbb.Common {
    export interface UserPreferenceRetrieveResponse extends Serenity.ServiceResponse {
        Value?: string;
    }
}

