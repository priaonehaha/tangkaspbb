﻿namespace tangkaspbb.Organisasi {
    export interface InstitutionsRow {
        Institutionid?: number;
        Institutionname?: string;
        Level?: number;
        Super?: number;
        SuperInstitutionname?: string;
        SuperLevel?: number;
        Super1?: number;
    }

    export namespace InstitutionsRow {
        export const idProperty = 'Institutionid';
        export const nameProperty = 'Institutionname';
        export const localTextPrefix = 'Organisasi.Institutions';

        export declare const enum Fields {
            Institutionid = "Institutionid",
            Institutionname = "Institutionname",
            Level = "Level",
            Super = "Super",
            SuperInstitutionname = "SuperInstitutionname",
            SuperLevel = "SuperLevel",
            Super1 = "Super1"
        }
    }
}
