﻿namespace tangkaspbb.Organisasi {
    export interface InstitutionsForm {
        Institutionname: Serenity.StringEditor;
        Level: Serenity.IntegerEditor;
        Super: Serenity.IntegerEditor;
    }

    export class InstitutionsForm extends Serenity.PrefixedContext {
        static formKey = 'Organisasi.Institutions';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!InstitutionsForm.init)  {
                InstitutionsForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.IntegerEditor;

                Q.initFormType(InstitutionsForm, [
                    'Institutionname', w0,
                    'Level', w1,
                    'Super', w1
                ]);
            }
        }
    }
}
