﻿namespace tangkaspbb.Organisasi {
    export interface PositionsForm {
        Positionname: Serenity.StringEditor;
        Institutionid: Serenity.IntegerEditor;
        Level: Serenity.IntegerEditor;
        Super: Serenity.IntegerEditor;
    }

    export class PositionsForm extends Serenity.PrefixedContext {
        static formKey = 'Organisasi.Positions';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!PositionsForm.init)  {
                PositionsForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.IntegerEditor;

                Q.initFormType(PositionsForm, [
                    'Positionname', w0,
                    'Institutionid', w1,
                    'Level', w1,
                    'Super', w1
                ]);
            }
        }
    }
}
