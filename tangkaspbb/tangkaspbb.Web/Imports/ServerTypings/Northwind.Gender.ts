﻿namespace tangkaspbb.Northwind {
    export enum Gender {
        Male = 1,
        Female = 2
    }
    Serenity.Decorators.registerEnumType(Gender, 'tangkaspbb.Northwind.Gender', 'tangkaspbb.Northwind.Entities.Gender');
}
