﻿namespace tangkaspbb.Dokumen {
    export interface SignaturesForm {
        Signaturename: Serenity.StringEditor;
        Isvisiblesignature: Serenity.StringEditor;
        Imagepath: Serenity.StringEditor;
        Bgpath: Serenity.StringEditor;
        Lowerx: Serenity.IntegerEditor;
        Lowery: Serenity.IntegerEditor;
        Upperx: Serenity.IntegerEditor;
        Uppery: Serenity.IntegerEditor;
        Possitiontype: Serenity.IntegerEditor;
        Formelementname: Serenity.StringEditor;
        Signaturetext: Serenity.StringEditor;
        Statustext: Serenity.StringEditor;
        Signaturelocation: Serenity.StringEditor;
        Pagenumber: Serenity.IntegerEditor;
        Rendermode: Serenity.IntegerEditor;
    }

    export class SignaturesForm extends Serenity.PrefixedContext {
        static formKey = 'Dokumen.Signatures';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!SignaturesForm.init)  {
                SignaturesForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.IntegerEditor;

                Q.initFormType(SignaturesForm, [
                    'Signaturename', w0,
                    'Isvisiblesignature', w0,
                    'Imagepath', w0,
                    'Bgpath', w0,
                    'Lowerx', w1,
                    'Lowery', w1,
                    'Upperx', w1,
                    'Uppery', w1,
                    'Possitiontype', w1,
                    'Formelementname', w0,
                    'Signaturetext', w0,
                    'Statustext', w0,
                    'Signaturelocation', w0,
                    'Pagenumber', w1,
                    'Rendermode', w1
                ]);
            }
        }
    }
}
