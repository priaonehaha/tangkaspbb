﻿namespace tangkaspbb.Dokumen {
    export interface CertificatesForm {
        Certificatename: Serenity.StringEditor;
        Issuedto: Serenity.StringEditor;
        Issuedby: Serenity.StringEditor;
        Validfrom: Serenity.DateEditor;
        Validto: Serenity.DateEditor;
        Certificatetype: Serenity.IntegerEditor;
        Serialnumber: Serenity.StringEditor;
        Thumbprint: Serenity.StringEditor;
        Subjectcn: Serenity.StringEditor;
        Subjecto: Serenity.StringEditor;
        Subjectl: Serenity.StringEditor;
        Subjects: Serenity.StringEditor;
        Subjectc: Serenity.StringEditor;
        Tsaconfigid: Serenity.IntegerEditor;
        Ocspenable: Serenity.StringEditor;
        Ocspserver: Serenity.StringEditor;
        Keystorepath: Serenity.StringEditor;
        Keystoretype: Serenity.IntegerEditor;
        Hashalgorithm: Serenity.IntegerEditor;
        Tsaurl: Serenity.StringEditor;
        Tsaauthtype: Serenity.IntegerEditor;
        Authusername: Serenity.StringEditor;
        Authpassword: Serenity.StringEditor;
        Tsahashalgorithm: Serenity.StringEditor;
    }

    export class CertificatesForm extends Serenity.PrefixedContext {
        static formKey = 'Dokumen.Certificates';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!CertificatesForm.init)  {
                CertificatesForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.DateEditor;
                var w2 = s.IntegerEditor;

                Q.initFormType(CertificatesForm, [
                    'Certificatename', w0,
                    'Issuedto', w0,
                    'Issuedby', w0,
                    'Validfrom', w1,
                    'Validto', w1,
                    'Certificatetype', w2,
                    'Serialnumber', w0,
                    'Thumbprint', w0,
                    'Subjectcn', w0,
                    'Subjecto', w0,
                    'Subjectl', w0,
                    'Subjects', w0,
                    'Subjectc', w0,
                    'Tsaconfigid', w2,
                    'Ocspenable', w0,
                    'Ocspserver', w0,
                    'Keystorepath', w0,
                    'Keystoretype', w2,
                    'Hashalgorithm', w2,
                    'Tsaurl', w0,
                    'Tsaauthtype', w2,
                    'Authusername', w0,
                    'Authpassword', w0,
                    'Tsahashalgorithm', w0
                ]);
            }
        }
    }
}
