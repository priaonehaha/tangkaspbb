﻿namespace tangkaspbb.Dokumen {
    export interface DocumenttypesRow {
        Documenttypeid?: number;
        Documenttypename?: string;
        Outputsuffix?: string;
        Outputprefix?: string;
        Isdisableassembly?: number[];
        Isdisablecopy?: number[];
        Isdisablefill?: number[];
        Isdisablemodifyannotations?: number[];
        Isdisablemodifycontent?: number[];
        Isdisablescreenreaders?: number[];
        Certificationlevel?: number;
        Officialid?: number;
        Printright?: number;
        Certificateid?: number;
        Signatureid?: number;
        Isencrypted?: number[];
        Userpassword?: string;
        Ownerpassword?: string;
        Institutionid?: number;
        OfficialidOfficialname?: string;
        OfficialidInstitutionid?: number;
        OfficialidServedfrom?: string;
        OfficialidServedto?: string;
        OfficialidCertificateid?: number;
        OfficialidPositionid?: number;
        OfficialidSignatureid?: number;
        CertificateidCertificatename?: string;
        CertificateidIssuedto?: string;
        CertificateidIssuedby?: string;
        CertificateidValidfrom?: string;
        CertificateidValidto?: string;
        CertificateidCertificatetype?: number;
        CertificateidSerialnumber?: string;
        CertificateidThumbprint?: string;
        CertificateidSubjectcn?: string;
        CertificateidSubjecto?: string;
        CertificateidSubjectl?: string;
        CertificateidSubjects?: string;
        CertificateidSubjectc?: string;
        CertificateidTsaconfigid?: number;
        CertificateidOcspenable?: number[];
        CertificateidOcspserver?: string;
        CertificateidKeystorepath?: string;
        CertificateidKeystoretype?: number;
        CertificateidHashalgorithm?: number;
        CertificateidTsaurl?: string;
        CertificateidTsaauthtype?: number;
        CertificateidAuthusername?: string;
        CertificateidAuthpassword?: string;
        CertificateidTsahashalgorithm?: string;
        SignatureidSignaturename?: string;
        SignatureidIsvisiblesignature?: number[];
        SignatureidImagepath?: string;
        SignatureidBgpath?: string;
        SignatureidLowerx?: number;
        SignatureidLowery?: number;
        SignatureidUpperx?: number;
        SignatureidUppery?: number;
        SignatureidPossitiontype?: number;
        SignatureidFormelementname?: string;
        SignatureidSignaturetext?: string;
        SignatureidStatustext?: string;
        SignatureidSignaturelocation?: string;
        SignatureidPagenumber?: number;
        SignatureidRendermode?: number;
        InstitutionidInstitutionname?: string;
        InstitutionidLevel?: number;
        InstitutionidSuper?: number;
    }

    export namespace DocumenttypesRow {
        export const idProperty = 'Documenttypeid';
        export const nameProperty = 'Documenttypename';
        export const localTextPrefix = 'Dokumen.Documenttypes';

        export declare const enum Fields {
            Documenttypeid = "Documenttypeid",
            Documenttypename = "Documenttypename",
            Outputsuffix = "Outputsuffix",
            Outputprefix = "Outputprefix",
            Isdisableassembly = "Isdisableassembly",
            Isdisablecopy = "Isdisablecopy",
            Isdisablefill = "Isdisablefill",
            Isdisablemodifyannotations = "Isdisablemodifyannotations",
            Isdisablemodifycontent = "Isdisablemodifycontent",
            Isdisablescreenreaders = "Isdisablescreenreaders",
            Certificationlevel = "Certificationlevel",
            Officialid = "Officialid",
            Printright = "Printright",
            Certificateid = "Certificateid",
            Signatureid = "Signatureid",
            Isencrypted = "Isencrypted",
            Userpassword = "Userpassword",
            Ownerpassword = "Ownerpassword",
            Institutionid = "Institutionid",
            OfficialidOfficialname = "OfficialidOfficialname",
            OfficialidInstitutionid = "OfficialidInstitutionid",
            OfficialidServedfrom = "OfficialidServedfrom",
            OfficialidServedto = "OfficialidServedto",
            OfficialidCertificateid = "OfficialidCertificateid",
            OfficialidPositionid = "OfficialidPositionid",
            OfficialidSignatureid = "OfficialidSignatureid",
            CertificateidCertificatename = "CertificateidCertificatename",
            CertificateidIssuedto = "CertificateidIssuedto",
            CertificateidIssuedby = "CertificateidIssuedby",
            CertificateidValidfrom = "CertificateidValidfrom",
            CertificateidValidto = "CertificateidValidto",
            CertificateidCertificatetype = "CertificateidCertificatetype",
            CertificateidSerialnumber = "CertificateidSerialnumber",
            CertificateidThumbprint = "CertificateidThumbprint",
            CertificateidSubjectcn = "CertificateidSubjectcn",
            CertificateidSubjecto = "CertificateidSubjecto",
            CertificateidSubjectl = "CertificateidSubjectl",
            CertificateidSubjects = "CertificateidSubjects",
            CertificateidSubjectc = "CertificateidSubjectc",
            CertificateidTsaconfigid = "CertificateidTsaconfigid",
            CertificateidOcspenable = "CertificateidOcspenable",
            CertificateidOcspserver = "CertificateidOcspserver",
            CertificateidKeystorepath = "CertificateidKeystorepath",
            CertificateidKeystoretype = "CertificateidKeystoretype",
            CertificateidHashalgorithm = "CertificateidHashalgorithm",
            CertificateidTsaurl = "CertificateidTsaurl",
            CertificateidTsaauthtype = "CertificateidTsaauthtype",
            CertificateidAuthusername = "CertificateidAuthusername",
            CertificateidAuthpassword = "CertificateidAuthpassword",
            CertificateidTsahashalgorithm = "CertificateidTsahashalgorithm",
            SignatureidSignaturename = "SignatureidSignaturename",
            SignatureidIsvisiblesignature = "SignatureidIsvisiblesignature",
            SignatureidImagepath = "SignatureidImagepath",
            SignatureidBgpath = "SignatureidBgpath",
            SignatureidLowerx = "SignatureidLowerx",
            SignatureidLowery = "SignatureidLowery",
            SignatureidUpperx = "SignatureidUpperx",
            SignatureidUppery = "SignatureidUppery",
            SignatureidPossitiontype = "SignatureidPossitiontype",
            SignatureidFormelementname = "SignatureidFormelementname",
            SignatureidSignaturetext = "SignatureidSignaturetext",
            SignatureidStatustext = "SignatureidStatustext",
            SignatureidSignaturelocation = "SignatureidSignaturelocation",
            SignatureidPagenumber = "SignatureidPagenumber",
            SignatureidRendermode = "SignatureidRendermode",
            InstitutionidInstitutionname = "InstitutionidInstitutionname",
            InstitutionidLevel = "InstitutionidLevel",
            InstitutionidSuper = "InstitutionidSuper"
        }
    }
}
