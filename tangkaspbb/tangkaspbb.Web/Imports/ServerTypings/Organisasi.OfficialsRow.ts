﻿namespace tangkaspbb.Organisasi {
    export interface OfficialsRow {
        Officialid?: number;
        Officialname?: string;
        Institutionid?: number;
        Servedfrom?: string;
        Servedto?: string;
        Certificateid?: number;
        Positionid?: number;
        Signatureid?: number;
        InstitutionidInstitutionname?: string;
        InstitutionidLevel?: number;
        InstitutionidSuper?: number;
        PositionidPositionname?: string;
        PositionidInstitutionid?: number;
        PositionidLevel?: number;
        PositionidSuper?: number;
    }

    export namespace OfficialsRow {
        export const idProperty = 'Officialid';
        export const nameProperty = 'Officialname';
        export const localTextPrefix = 'Organisasi.Officials';

        export declare const enum Fields {
            Officialid = "Officialid",
            Officialname = "Officialname",
            Institutionid = "Institutionid",
            Servedfrom = "Servedfrom",
            Servedto = "Servedto",
            Certificateid = "Certificateid",
            Positionid = "Positionid",
            Signatureid = "Signatureid",
            InstitutionidInstitutionname = "InstitutionidInstitutionname",
            InstitutionidLevel = "InstitutionidLevel",
            InstitutionidSuper = "InstitutionidSuper",
            PositionidPositionname = "PositionidPositionname",
            PositionidInstitutionid = "PositionidInstitutionid",
            PositionidLevel = "PositionidLevel",
            PositionidSuper = "PositionidSuper"
        }
    }
}
