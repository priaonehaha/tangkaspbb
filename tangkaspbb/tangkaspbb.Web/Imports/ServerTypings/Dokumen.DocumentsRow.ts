﻿namespace tangkaspbb.Dokumen {
    export interface DocumentsRow {
        Documentsid?: number;
        Documenttypeid?: number;
        Documentname?: string;
        Documentdescription?: string;
        Originalfilename?: string;
        Originalfilepath?: string;
        Uploadby?: number;
        Uploadedtimestamp?: string;
        Savedpath?: string;
        Savedfilename?: string;
        Documentstatus?: string;
        Processstarttimestamp?: string;
        Processendtimestamp?: string;
        Processedby?: number;
        Processstatus?: string;
        DocumenttypeidDocumenttypename?: string;
        DocumenttypeidOutputsuffix?: string;
        DocumenttypeidOutputprefix?: string;
        DocumenttypeidIsdisableassembly?: number[];
        DocumenttypeidIsdisablecopy?: number[];
        DocumenttypeidIsdisablefill?: number[];
        DocumenttypeidIsdisablemodifyannotations?: number[];
        DocumenttypeidIsdisablemodifycontent?: number[];
        DocumenttypeidIsdisablescreenreaders?: number[];
        DocumenttypeidCertificationlevel?: number;
        DocumenttypeidOfficialid?: number;
        DocumenttypeidPrintright?: number;
        DocumenttypeidCertificateid?: number;
        DocumenttypeidSignatureid?: number;
        DocumenttypeidIsencrypted?: number[];
        DocumenttypeidUserpassword?: string;
        DocumenttypeidOwnerpassword?: string;
        DocumenttypeidInstitutionid?: number;
    }

    export namespace DocumentsRow {
        export const idProperty = 'Documentsid';
        export const nameProperty = 'Documentname';
        export const localTextPrefix = 'Dokumen.Documents';

        export declare const enum Fields {
            Documentsid = "Documentsid",
            Documenttypeid = "Documenttypeid",
            Documentname = "Documentname",
            Documentdescription = "Documentdescription",
            Originalfilename = "Originalfilename",
            Originalfilepath = "Originalfilepath",
            Uploadby = "Uploadby",
            Uploadedtimestamp = "Uploadedtimestamp",
            Savedpath = "Savedpath",
            Savedfilename = "Savedfilename",
            Documentstatus = "Documentstatus",
            Processstarttimestamp = "Processstarttimestamp",
            Processendtimestamp = "Processendtimestamp",
            Processedby = "Processedby",
            Processstatus = "Processstatus",
            DocumenttypeidDocumenttypename = "DocumenttypeidDocumenttypename",
            DocumenttypeidOutputsuffix = "DocumenttypeidOutputsuffix",
            DocumenttypeidOutputprefix = "DocumenttypeidOutputprefix",
            DocumenttypeidIsdisableassembly = "DocumenttypeidIsdisableassembly",
            DocumenttypeidIsdisablecopy = "DocumenttypeidIsdisablecopy",
            DocumenttypeidIsdisablefill = "DocumenttypeidIsdisablefill",
            DocumenttypeidIsdisablemodifyannotations = "DocumenttypeidIsdisablemodifyannotations",
            DocumenttypeidIsdisablemodifycontent = "DocumenttypeidIsdisablemodifycontent",
            DocumenttypeidIsdisablescreenreaders = "DocumenttypeidIsdisablescreenreaders",
            DocumenttypeidCertificationlevel = "DocumenttypeidCertificationlevel",
            DocumenttypeidOfficialid = "DocumenttypeidOfficialid",
            DocumenttypeidPrintright = "DocumenttypeidPrintright",
            DocumenttypeidCertificateid = "DocumenttypeidCertificateid",
            DocumenttypeidSignatureid = "DocumenttypeidSignatureid",
            DocumenttypeidIsencrypted = "DocumenttypeidIsencrypted",
            DocumenttypeidUserpassword = "DocumenttypeidUserpassword",
            DocumenttypeidOwnerpassword = "DocumenttypeidOwnerpassword",
            DocumenttypeidInstitutionid = "DocumenttypeidInstitutionid"
        }
    }
}
