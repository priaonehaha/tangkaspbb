﻿namespace tangkaspbb.Dokumen {
    export interface DocumentsForm {
        Documenttypeid: Serenity.IntegerEditor;
        Documentname: Serenity.StringEditor;
        Documentdescription: Serenity.StringEditor;
        Originalfilename: Serenity.StringEditor;
        Originalfilepath: Serenity.StringEditor;
        Uploadby: Serenity.IntegerEditor;
        Uploadedtimestamp: Serenity.DateEditor;
        Savedpath: Serenity.StringEditor;
        Savedfilename: Serenity.StringEditor;
        Documentstatus: Serenity.StringEditor;
        Processstarttimestamp: Serenity.DateEditor;
        Processendtimestamp: Serenity.DateEditor;
        Processedby: Serenity.IntegerEditor;
        Processstatus: Serenity.StringEditor;
    }

    export class DocumentsForm extends Serenity.PrefixedContext {
        static formKey = 'Dokumen.Documents';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!DocumentsForm.init)  {
                DocumentsForm.init = true;

                var s = Serenity;
                var w0 = s.IntegerEditor;
                var w1 = s.StringEditor;
                var w2 = s.DateEditor;

                Q.initFormType(DocumentsForm, [
                    'Documenttypeid', w0,
                    'Documentname', w1,
                    'Documentdescription', w1,
                    'Originalfilename', w1,
                    'Originalfilepath', w1,
                    'Uploadby', w0,
                    'Uploadedtimestamp', w2,
                    'Savedpath', w1,
                    'Savedfilename', w1,
                    'Documentstatus', w1,
                    'Processstarttimestamp', w2,
                    'Processendtimestamp', w2,
                    'Processedby', w0,
                    'Processstatus', w1
                ]);
            }
        }
    }
}
